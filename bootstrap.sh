#!/usr/bin/env bash

## Parameters
## ----------
#PHP_TIMEZONE="Europe/Berlin"
PHP_XDEBUG=1
MYSQL_USERNAME="vagrant"
MYSQL_PASSWORD="v4gr4n7"
MYSQL_DATABASE_NAME="dev_db"
APACHE2_DOCROOT="/vagrant/www"

## Update the box
## --------------
## Downloads the package lists from the repositories
## and "updates" them to get information on the newest
## versions of packages and their dependencies
apt-get update

## Packages
## --------
source /vagrant/.bootstrap/apache2
source /vagrant/.bootstrap/php56x
source /vagrant/.bootstrap/php5x-extras
source /vagrant/.bootstrap/mysql55
source /vagrant/.bootstrap/essentials
#source /vagrant/.bootstrap/composer
#source /vagrant/.bootstrap/compass

## Init Composer Project
## ---------------------
if  [ -a /vagrant/www/composer.json ] && [ `which composer` ]
then
    cd /vagrant/www
    composer install --dev
fi

## Import drush-archive.tar if present.
## ------------------------------------
if [ `which drush` ] && [ -a /vagrant/drush-archive.tar ] && [ ! -a /vagrant/drush-import.lock ]
then
    drush arr /vagrant/drush-archive.tar --destination=$APACHE2_DOCROOT --overwrite --db-url=mysql://$MYSQL_USERNAME:$MYSQL_PASSWORD@localhost/$MYSQL_DATABASE_NAME
    drush cc all
    touch drush-import.lock
fi
Artus Vagrant (Base)
====================

A simple Vagrant template to be ready for PHP/MySQL development.

## Features:

### Enabled

* Apache2
* PHP 5.6.x (latest stable release)
* PHP packages : php5-cli php5-mysql php5-curl php5-mcrypt php5-gd php5-xdebug php5-intl
* MySQL 5.5 Server/Client
* Some essential packages : git-core vim curl 

### Additional
* NodeJS
* Nginx 

## Forwarded ports:

* 20 (SSH) > 2222
* 80 (HTTP) > 8080
* 9000 (xDebug) > 9001

## Installation

### Install box

With Vagrant version >= 1.3.4
`vagrant up` and `vagrant provision` (if you have puppet or chef provision, you can run the command with the flag `--provision-with shell` to launch only shell provisioner)

or with Vagrant version < 1.3.4
`vagrant up`

### Stop box

`vagrant halt`

### Reload box (not install, only reboot after stopping)

With Vagrant version >= 1.3.4
`vagrant reload`

or with Vagrant version < 1.3.4
`vagrant reload --no-provision`

### Access to your web application

Simply go to your favorite browser and type this URL : `http://localhost:8080` !

## Bootstrap and box parameters

### Bootstrap configuration

You need to edit the parameters section in the file "bootstrap.sh" :  

Database parameters:

* `MYSQL_USERNAME` : Your mysql username. If empty, no user will be created.
* `MYSQL_PASSWORD` : Your mysql password. If empty, no database will be created. Requires `MYSQL_USERNAME`
* `MYSQL_DATABASE_NAME` : Your mysql database name. If empty, no database will be created.

PHP parameters:

* `PHP_XDEBUG` : Define if xdebug should be used (default: 0).
* `PHP_TIMEZONE` : The PHP timezone (default: "UTC"). Check possible values here : http://php.net/manual/en/timezones.php **(Not yet implemented)**